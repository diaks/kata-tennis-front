// js/todoList.js
'use strict';

var app = angular.module('app', []);

app.controller('appCtrl', ['$http', '$scope',

  function($http, $scope) {

    console.log("init app controller...");
    $http.get('http://localhost:8080/kata-tennis/game/view').then(
      function(response) {
        $scope.computerMode = false;
        $scope.game = null;
        $scope.scoresLength = null;
        $scope.game = response.data;

        $scope.playerScored = function(idPlayer) {
          updateScore(idPlayer);
        };


        $scope.newGame = function() {
          $http.get('http://localhost:8080/kata-tennis/game/new_game/').then(
            function(response) {
              $scope.game = response.data;
            },
            function(error) {
              console.log(error);
            }
          );
        };

        $scope.runComputerMode = function() {
          var idPlayer = 2;
          setInterval(function() {
            if (idPlayer == Math.random(1, 3)) {
              updateScore(idPlayer);
              idPlayer = 2;
            } else {
              updateScore(idPlayer);
              idPlayer = 1;
            }
          }, 2000);
          $scope.computerMode = true;

        };

        function updateScore(idPlayer) {
          $http.get('http://localhost:8080/kata-tennis/game/update_score/' + idPlayer).then(
            function(response) {
              $scope.game = response.data;
            },
            function(error) {
              console.log(error);
            }
          );
        }

      },
      function(error) {
        console.log(error);
      }
    );


  }

]);
